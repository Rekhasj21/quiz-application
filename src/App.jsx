import React, { Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

const DifficultyLevel = React.lazy(() =>
  import("./components/Levels/DifficultyLevel")
);

const QuizLevelsHomePage = React.lazy(() =>
  import("./components/QuizLevels/QuizLevelsHomePage")
);

const HomePage = React.lazy(() => import("./components/Home/HomePage"));
const Navbar = React.lazy(() => import("./components/Navbar/Navbar"));
const NotFound = React.lazy(() => import("./components/NotFound/NotFound"));
const ResultsDisplayPage = React.lazy(()=>import("./components/DisplayPage/ResultsDisplayPage"))

function App() {
  return (
    <BrowserRouter>
      <Suspense
        fallback={
          <div className="spinner-border text-center" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        }
      >
        <Navbar />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route
            path="/quiz/:name/:categoryId"
            element={<QuizLevelsHomePage />}
          />
          <Route
            path="/quiz/:name/:categoryId/:difficultyLevel"
            element={<DifficultyLevel />}
          />
          <Route path="/quiz/results" element={<ResultsDisplayPage />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
