import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  answerList: [],
  quizDifficultyLevelListData: [],
  isFetching: false,
  fetchError: "",
};

export const getdificultyLevelData = createAsyncThunk(
  "answer/getdificultyLevelData",
  async (name, asyncThunk) => {
    try {
      const response = await axios.get(
        `https://opentdb.com/api.php?amount=10&category=${name.categoryId}&difficulty=${name.difficultyLevel}`
      );
      return response.data.results;
    } catch (error) {
      return asyncThunk.rejectWithValue("Something went wrong.Please try again!");
    }
  }
);

const QuizAnswerSlice = createSlice({
  name: "answer",
  initialState,
  reducers: {
    getanswerList: (state, { payload }) => {
      const index = state.answerList.findIndex(
        (answer) => answer.question === payload.question
      );
      if (index === -1) {
        state.answerList.push(payload);
      } else {
        state.answerList[index].answer = payload.answer;
        state.answerList[index].correct_answer = payload.correct_answer;
      }
    },
    clearAnswers: (state) => {
      state.answerList = [];
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getdificultyLevelData.pending, (state) => {
      state.isFetching = true;
    }),
      builder.addCase(getdificultyLevelData.fulfilled, (state, { payload }) => {
        (state.isFetching = false),
          (state.quizDifficultyLevelListData = payload);
      }),
      builder.addCase(getdificultyLevelData.rejected, (state, { payload }) => {
        state.isFetching = false;
        state.fetchError = payload;
      });
  },
});

export const { getanswerList, clearAnswers } = QuizAnswerSlice.actions;
export default QuizAnswerSlice.reducer;
