import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  quizList: {},
  isLoading: false,
  error: "",
};

export const getQuizData = createAsyncThunk(
  "quiz/getAnimalsQuizData",
  async (name, thunkAPI) => {
    try {
      const response = await axios.get(
        `https://opentdb.com/api.php?amount=10&category=${name}`
      );
      return response.data.results;
    } catch (error) {
      console.log(error);
      thunkAPI.rejectWithValue("Something went wrong");
    }
  }
);

const QuizSlice = createSlice({
  name: "quiz",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getQuizData.pending, (state) => {
      state.isLoading = true;
    }),
      builder.addCase(getQuizData.fulfilled, (state, { payload }) => {
        (state.isLoading = false), (state.quizList = payload);
      }),
      builder.addCase(getQuizData.rejected, (state, { payload }) => {
        state.isLoading = false;
        state.error = payload;
      });
  },
});

export const {} = QuizSlice.actions;
export default QuizSlice.reducer;
