import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getQuizData } from "../Redux/QuizSlice";

function QuizLevelsHomePage() {
  const { isLoading } = useSelector((store) => store.quiz);
  const dispatch = useDispatch();
  const { name, categoryId } = useParams();

  useEffect(() => {
    dispatch(getQuizData(categoryId));
  }, []);

  return (
    <>
      {isLoading ? (
        <div className="d-flex justify-content-center m-5">
          <div className="spinner-border text-success spinner" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      ) : (
        <>
          <div className="d-flex flex-column justify-content-center align-items-center p-4">
            <h1 className="text-primary fw-bold fs-2">Welcome to {name} Quiz</h1>
            <p className="text-dark fw-bold fs-4 mt-4 mb-1 text-center">
              Here we have three difficulty levels,go with each one and test
              your knowledge
            </p>
            <p className="text-danger fw-bold fs-4">
              Select the level of Quiz
            </p>
          </div>
          <div className="d-flex flex-column justify-content-center align-items-center">
            <div className="card bg-warning bg-opacity-75 text-center hover-effect shadow border-0 w-50 m-3">
              <div className="card-body">
                <h5 className="card-title text-success fs-3 fw-bold">Easy</h5>
                <h6 className="card-subtitle mb-3 mt-3 text-dark fs-4 ">
                  Take this quiz to test knowledge on {name}
                </h6>
                <Link to={`/quiz/${name}/${categoryId}/easy`}>
                  <button className="btn btn-dark text-light pe-4 ps-4 hover-bg-green mb-3 fs-5">
                    Take this Quiz
                  </button>
                </Link>
              </div>
            </div>
            <div className="card bg-warning bg-opacity-75 text-center hover-effect shadow border-0 w-50 m-3">
              <div className="card-body">
                <h5 className="card-title text-primary fs-3 fw-bold">Medium</h5>
                <h6 className="card-subtitle mb-3 mt-3 text-body-secondary fs-4">
                  Take this quiz to test knowledge on {name}
                </h6>
                <Link to={`/quiz/${name}/${categoryId}/medium`}>
                  <button className="btn btn-dark text-light pe-4 ps-4 hover-bg-green mb-3 fs-5">
                    Take this Quiz
                  </button>
                </Link>
              </div>
            </div>
            <div className="card bg-warning bg-opacity-75 text-center hover-effect shadow border-0 w-50 m-3">
              <div className="card-body">
                <h5 className="card-title text-danger fs-3 fw-bold">Hard</h5>
                <h6 className="card-subtitle mb-3 mt-3 fs-4 text-body-secondary">
                  Take this quiz to test knowledge on {name}
                </h6>
                <Link to={`/quiz/${name}/${categoryId}/hard`}>
                  <button className="btn btn-dark text-light pe-4 ps-4 hover-bg-red mb-4 fs-5">
                    Take this Quiz
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default QuizLevelsHomePage;
