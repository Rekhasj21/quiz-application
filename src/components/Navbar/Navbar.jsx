import React from "react";
import { Link } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";

function Navbar() {
  return (
    <nav className="navbar bg-body-tertiary bg-warning">
      <div className="container-fluid">
        <Link to="/" className="text-decoration-none navbar-brand fw-bold">
          <AiFillHome fontSize="40px" fill="green" />
        </Link>
        <Link to="/" className="text-decoration-none navbar-brand fs-3 fw-bold">
          Quiz App
        </Link>
      </div>
    </nav>
  );
}

export default Navbar;
