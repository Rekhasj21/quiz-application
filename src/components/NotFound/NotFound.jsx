import React from "react";
import {Link} from 'react-router-dom'

function NotFound() {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center m-5">
      <img className="w-50" alt="not found" src="/not-found.jpg" />
      <Link to='/'>
        <button className="btn btn-primary m-5">Go Back To Home Page</button>
      </Link>
    </div>
  );
}

export default NotFound;
