import React from "react";
import ListDisplayPage from "../DisplayPage/ListDisplayPage";

const displayListOfQuizs = [
  {
    id: 1,
    name: "Animals",
    imageUrl: "/animal1.jpeg",
    category:27,
  },
  {
    id: 2,
    name: "Cartoons",
    imageUrl: "/cartoon1.jpeg",
    category:32
  },
  {
    id: 3,
    name: "Gadgets",
    imageUrl: "/mobile1.jpeg",
    category:30
  },
  {
    id: 4,
    name: "Vehicles",
    imageUrl: "/quiz.jpeg",
    category:28
  },
  {
    id: 5,
    name: "Comics",
    imageUrl: "/comics.jpeg",
    category:29
  },
  {
    id:6,
    name:"Mythology",
    imageUrl:'/mythology.jpeg',
    category:20
  }
];

function HomePage() {
  return (
    <>
      <h1 className="text-danger text-opacity-100 text-center fs-2 fw-bold mt-4">
        Welcome to Quiz Application
      </h1>
      <div className="d-flex flex-wrap justify-content-center align-items-center">
        {displayListOfQuizs.map((list) => {
          return <ListDisplayPage key={list.id} {...list} />;
        })}
      </div>
    </>
  );
}

export default HomePage;
