import React from "react";

class ErrorBoundary extends React.Component {
  state = { hasError: false };

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      return <h1 className="text-center text-danger fs-4 m-4">Check for errors and try again</h1>;
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
