import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import DifficultyLevelDisplayPage from "../DisplayPage/DifficultyLevelDisplayPage";
import { getanswerList, getdificultyLevelData } from "../Redux/QuizAnswerSlice";

function DifficultyLevel() {
  const { answerList, quizDifficultyLevelListData, isFetching,fetchError } = useSelector(
    (store) => store.answer
  );

  const { difficultyLevel, name, categoryId } = useParams();
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getdificultyLevelData({ name, categoryId, difficultyLevel }));
  }, []);

  const handleAnswerSelected = (answer) => {
    dispatch(getanswerList(answer));
  };

  const handleNextQuestion = () => {
    if (answerList[currentQuestion] === undefined) {
      alert("Please select one option");
      return;
    }
    setCurrentQuestion(currentQuestion + 1);
  };

  let totalQuiz = quizDifficultyLevelListData.length;
  const percentage = Math.round(((currentQuestion + 1) / totalQuiz) * 100);
  const progressStyle = { width: `${percentage}%` };

  let title = name.charAt(0).toUpperCase() + name.slice(1);
  let level =
    difficultyLevel.charAt(0).toUpperCase() + difficultyLevel.slice(1);
  const isLastQuestion =
    currentQuestion === quizDifficultyLevelListData.length - 1;

  return (
    <>
      {isFetching ? (
        <div className="d-flex justify-content-center m-4">
          <div className="spinner-border spinner" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      ) : (
        <>
        {(quizDifficultyLevelListData.length === 0) ? (
          <h1 className="text-danger text-center m-4">Something went wrong.Please try again!</h1>
        ):
        <>
          <div className="d-flex justify-content-around m-5">
            <h1 className="bg-success text-white fs-4 w-25 text-center p-3 ps-3 pe-3">
              Quiz Category:{` ${title}`}
            </h1>
            <h1 className="bg-success text-white fs-4 w-25 text-center p-3 ps-3 pe-3">
              Quiz Level:{` ${level}`}
            </h1>
          </div>
          <div className="d-flex flex-column justify-content-center align-items-center">
            <div className=" w-75">
              <p className="ps-1 mb-1 text-secondary">
                Question
                <span>
                  {currentQuestion + 1}/{totalQuiz}
                </span>
              </p>
              <div className="progress">
                <div
                  className="progress-bar"
                  role="progressbar"
                  aria-label="Basic example"
                  style={progressStyle}
                  aria-valuenow={percentage}
                  aria-valuemin={0}
                  aria-valuemax={100}
                />
              </div>
            </div>
            <DifficultyLevelDisplayPage
              count={currentQuestion}
              {...quizDifficultyLevelListData[currentQuestion]}
              onAnswerSelected={handleAnswerSelected}
              currentQuestion={currentQuestion}
              onNextQuestion={handleNextQuestion}
              isLastQuestion={isLastQuestion}
            />
          </div>
          </>
        }
        </>
      )}
    </>
  );
}

export default DifficultyLevel;
