import React from "react";
import { Link } from "react-router-dom";

function ListDisplayPage({ name, imageUrl,category }) {
  return (
    <div className="card mb-4 shadow-lg border-0 hover-effect m-5">
      <div className="d-flex">
        <div className="col-md-4">
          <img
            src={imageUrl}
            className="img-fluid rounded-start image"
            alt={name}
          />
        </div>
        <div className="col-md-7 ms-2 pt-2 p-2">
          <h5 className="card-title m-1">Quiz on {name}</h5>
          <p className="card-text m-1 mt-2 text-primary">
            Take this quiz to test knowledge on <span className="text-success fw-bold">{name} </span>
          </p>
          <p className="card-text mt-3">
            <Link to={`/quiz/${name}/${category}`}>
              <button className="btn btn-warning pe-3 pt-2 pb-2 ps-3 hover-bg-green text-white fw-bold">
                Try It Out
              </button>
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}

export default ListDisplayPage;
