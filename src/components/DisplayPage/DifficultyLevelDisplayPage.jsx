import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function DifficultyLevelDisplayPage({
  incorrect_answers,
  correct_answer,
  question,
  category,
  difficulty,
  onAnswerSelected,
  currentQuestion,
  onNextQuestion,
  isLastQuestion,
}) {
  const [isSelected, setIsSelected] = useState("");
  const navigate = useNavigate();

  const handleAnswer = (event) => {
    const answer = event.target.dataset.answer;
    setIsSelected(answer);
    onAnswerSelected({
      question,
      correct_answer,
      answer,
      difficulty,
      category,
    });
  };

  const handleSubmit = () => {
    return navigate("/quiz/results");
  };

  return (
    <div className="ps-5 shadow p-5 m-3 mt-5 w-75">
      <h1 className="fs-4 fw-bold">
        {currentQuestion + 1}) {question}
      </h1>
      <div className="card w-50 border-0">
        <ul className="w-100 hover-bg">
          <li
            className={`fs-5 list-group-item text-white bg-danger bg-opacity-75 m-2 p-1 pt-2 pb-2 fw-bold ${
              isSelected === correct_answer && "bg-dark"
            }`}
            onClick={(event) => handleAnswer(event)}
            data-answer={correct_answer}
            key={1}
          >
            1. {correct_answer}
          </li>
          {incorrect_answers &&
            incorrect_answers.map((item, index) => (
              <li
                className={`fs-5 list-group-item text-white bg-danger bg-opacity-75 m-2 p-1 pt-2 pb-2 fw-bold ${
                  isSelected === item && "bg-dark"
                }`}
                key={index + 2}
                data-answer={item}
                onClick={(event) => handleAnswer(event)}
              >
                {index + 2}. {item}
              </li>
            ))}
        </ul>
      </div>
      <div className="text-end">
        {isLastQuestion ? (
          <button
            className="btn btn-dark hover-bg-green fs-5 fw-bold p-2 pe-3 ps-3"
            onClick={handleSubmit}
          >
            Submit
          </button>
        ) : (
          <button
            className="btn btn-dark hover-bg-green fs-5 fw-bold p-2 pe-3 ps-3"
            onClick={onNextQuestion}
          >
            Next Question
          </button>
        )}
      </div>
    </div>
  );
}

export default DifficultyLevelDisplayPage;
