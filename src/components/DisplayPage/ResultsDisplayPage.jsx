import { nanoid } from "@reduxjs/toolkit";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { clearAnswers } from "../Redux/QuizAnswerSlice";

function ResultsDisplayPage() {
  const { answerList } = useSelector((store) => store.answer);
  const dispatch = useDispatch();

  const handleData = () => {
    dispatch(clearAnswers());
  };

  let correctAnswerCount = answerList.filter(
    (list) => list.correct_answer === list.answer
  ).length;

  let level = answerList[0].difficulty[0].toUpperCase()+ answerList[0].difficulty.slice(1)

  return (
    <>
      <div className="text-center">
        <img alt="winner icon" src="/winner-icon.jpg" className="win-icon" />
        <h1 className="fs-4 fw-bold">Quiz Result</h1>
        <h2 className="fs-5 text-danger">
          Quiz Category: {answerList[0].category}
        </h2>
        <h2 className="fs-5 text-primary">
          Quiz Difficulty Level: {level}
        </h2>
        <h3 className="fs-5 text-success fw-bold">
          Final Score: {correctAnswerCount}
        </h3>
      </div>
      <div className="d-flex justify-content-center ">
        <table className="table table-bordered border-primary me-5 ms-5 w-75 text-center">
          <thead>
            <tr className="border-danger">
              <th scope="col fs-6">Question</th>
              <th scope="col">Correct Answers</th>
              <th scope="col">You Selected</th>
              <th scope="col">Right or Wrong</th>
            </tr>
          </thead>
          <tbody>
            {answerList.map((list) => (
              <tr className="border-success" key={nanoid()}>
                <td className="border-danger">{list.question}</td>
                <td className="border-danger">{list.correct_answer}</td>
                <td className="border-danger">{list.answer}</td>
                <td className="border-danger">
                  {list.correct_answer === list.answer ? (
                    <img src="/correct.jpeg" className="icon" />
                  ) : (
                    <img src="/wrong.png" className="icon" />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div className="text-center">
        <Link to="/">
          <button className="btn btn-primary m-3" onClick={handleData}>
            Go To Home Page
          </button>
        </Link>
      </div>
    </>
  );
}

export default ResultsDisplayPage;
