import { configureStore } from "@reduxjs/toolkit";
import QuizSliceReducer from "./components/Redux/QuizSlice";
import QuizAnswerReducer from "./components/Redux/QuizAnswerSlice";

const store = configureStore({
  reducer: {
    quiz: QuizSliceReducer,
    answer: QuizAnswerReducer,
  },
});

export default store;
