
<h1 align="center">Quiz Application</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="image text"><img src="public/quiz-app-home-page.png" alt="" width="300px"/></a>

- Can play Quiz based on category
- Each Category has three difficulty levels

## Project Link

<a href="https://rekha-sj-quiz-application.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- CSS
- Bootstrap
- React JS
- Redux Toolkit

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/quiz-application.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-sj-quiz-application.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/quiz-application>)